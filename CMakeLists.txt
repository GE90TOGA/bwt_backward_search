cmake_minimum_required(VERSION 3.6)
project(ass2)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -std=c++11 -Wall -Werror")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "/home/linus/cs9319/ass2")
set(SOURCE_FILES bwtsearch.h bwtsearch.cpp)
add_executable(ass2 ${SOURCE_FILES})




