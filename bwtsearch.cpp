#include <iostream>
#include <fstream>
#include <array>
#include <algorithm>
#include <tuple>
#include <list>
#include "bwtsearch.h"

bool BIG_FILE = true;
const int CONST_MAX_BLOCK_LEN = 20480;
int BlockLengthInByte; // 160 * 128
const int INDEX_ALPAHBET_SIZE = 98;
const char BUFFER_TERMIN = '\0';

int main(int argc, char *argv[]) {

    char *globalBuffer;
    int *Ctable;
    int *indexTable;
    int termCount = 1;

    std::string bwtFileName(argv[1]);
    std::string term1 = argv[3];
    std::string term2 = "";
    std::string term3 = "";
    if (argc > 4) {
        term2 = argv[4];
        ++termCount;
    }
    if (argc == 6) {
        term3 = argv[5];
        ++termCount;
    }

    std::vector<std::string> terms{term1, term2, term3};
    term1 = "";
    term2 = "";
    term3 = ""; // empty strings to save space

    std::ifstream is(bwtFileName, std::ios::in | std::ifstream::binary);
    int fLen = getFileLenByte(is);
    BlockLengthInByte = getBlockLengthToDivide(fLen);
    if (fLen <= BlockLengthInByte) {
        BIG_FILE = false;
    }
    // total number of blocks in bwt file
    int totalBlockNum = getTotalBlockNum(fLen, BlockLengthInByte);

    indexTable = new int[totalBlockNum * INDEX_ALPAHBET_SIZE];
    //initIndex(indexTable, totalBlockNum * INDEX_ALPAHBET_SIZE); // init all to 0
    initIntArray(indexTable, 0, totalBlockNum * INDEX_ALPAHBET_SIZE);

    globalBuffer = new char[BlockLengthInByte];
    // build index
    if (!BIG_FILE) { // non "big" file has no index, no need to check
        buildBlockIndex(0, globalBuffer, is, indexTable);
        //printCount(0, indexTable);
    } else {
        // check if index file exsits, if exists read from index file and skip build
        for (int blockIndex = 0; blockIndex < totalBlockNum; ++blockIndex) {
            buildBlockIndex(blockIndex, globalBuffer, is, indexTable);
        }
    }
    // build C
    Ctable = new int[INDEX_ALPAHBET_SIZE];
    initIntArray(Ctable, -1, INDEX_ALPAHBET_SIZE);
    // build C table based on the last block of index
    buildCtable(totalBlockNum - 1, indexTable, Ctable);
    // start backward search for the longest term
    auto res_1 = backWardSearch(terms[0], is, totalBlockNum, indexTable, Ctable, globalBuffer);
    auto res_2 = backWardSearch(terms[1], is, totalBlockNum, indexTable, Ctable, globalBuffer);
    if (termCount > 1 && std::get<0>(res_2) < 0) { // backward search returns nothing, no need to go further just stop
        return 0;
    }
    auto res_3 = backWardSearch(terms[2], is, totalBlockNum, indexTable, Ctable, globalBuffer);
    if (termCount > 2 && std::get<0>(res_3) < 0) { // backward search returns nothing, no need to go further just stop
        return 0;
    }
    auto decodeArea = getMinimumSearchArea(std::get<0>(res_1), std::get<1>(res_1),
                                           std::get<0>(res_2), std::get<1>(res_2),
                                           std::get<0>(res_3), std::get<1>(res_3));
    int seek_posi = std::get<0>(decodeArea);
    const int last_seek = std::get<1>(decodeArea);
    int searchedTermIndex = std::get<2>(decodeArea);
    if (searchedTermIndex >= 0) {
        terms.erase(std::begin(terms) + searchedTermIndex);
    } else{
        return 0;
    }
    if (seek_posi < 0 || last_seek < 0) { // return no match
        return 0;
    }
    while (seek_posi <= last_seek) {
        // bacward decode
        std::string resultStr = "";
        resultStr = backwardDecode(seek_posi, is, Ctable, globalBuffer, indexTable);
        if (resultStr == "") { // if "" from backward skip (continue)
            ++seek_posi;
            continue;
        }
        // forward
        // concat b+f
        resultStr += forwardDecode(seek_posi, totalBlockNum, is, Ctable, globalBuffer, indexTable);
        // substring_1 check number
        // skip if no result
        if (terms[0] != "") {
            if (subStringMatch(resultStr, terms[0])) {
                // 2nd string matches, check 3rd
                if (terms[1] != "") {
                    if (subStringMatch(resultStr, terms[1])) {
                        std::cout << resultStr << std::endl;
                    }
                } else { // 3rd empty just output since 2nd matches
                    std::cout << resultStr << std::endl;
                }
            }
        } else { // 2nd empty just output
            std::cout << resultStr << std::endl;
        }
        ++seek_posi;
    }

    delete[] Ctable;
    delete[] globalBuffer;
    delete[] indexTable;
    is.close();

}

/**
 * The search term should not contain [ or ], if there is a term contains these, just return no result
 * @param s
 * @return
 */
bool validateString(const std::string &s) {
    auto fail_1 = s.find("[");
    auto fail_2 = s.find("]");
    if (fail_1 != std::string::npos) { // find [ or ] in term just fail the test
        return false;
    }
    if (fail_2 != std::string::npos) {
        return false;
    }
    return true; // ok to continue

}

/**
 * return the proper block length for different scenarios (len <= 1024B, 1024< len <= 40960B and len > 40960B)
 * @param fLen
 * @return
 */
int getBlockLengthToDivide(const int fLen) {
    if (fLen <= 1024) {
        return 1024;

    } else if (fLen > 1024 && fLen <= CONST_MAX_BLOCK_LEN * 2) {
        return 100; // chop every 100 char into a block result in max 0.16MB index in memory
    }
    return CONST_MAX_BLOCK_LEN;
}

/**
 * Init int array with default value
 * @param array
 * @param defaultValue
 * @param len
 */
void initIntArray(int *array, int defaultValue, int len) {
    for (int i = 0; i != len; ++i) { array[i] = defaultValue; }
}

void resetBuffer(char *buffer, int len) {
    for (int i = 0; i != len; ++i) { buffer[i] = '\0'; }
}

int getFileLenByte(std::ifstream &in) {
    in.seekg(0, in.end);
    return in.tellg();
}


/**
 * Read a stream of file, put content into buffer
 * @param buffer the buffer that is going to contain the chars read from file
 * @param in the input stream
 * @param offset the position in file to start reading
 * @param bufferSize the length of reading, should be equal to buffer
 */
void readToBuffer(char *buffer, std::ifstream &in, int offset, int bufferSize) {
    // clear buffer
    resetBuffer(buffer, BlockLengthInByte);
    in.seekg(offset, in.beg);
    in.read(buffer, bufferSize);
    in.clear();
}

/**
 * Read A single character from file based on absolute position
 * @param in
 * @param posi
 * @return
 */
char readCharInfile(std::ifstream &in, int absPosi) {
    in.seekg(absPosi, in.beg);
    char c[1];
    in.read(c, 1);
    in.clear();
    return c[0];
}

/**
 * Return the total number of block of in index file for the given original file length in byte
 * @param fileLenByte
 * @param blockLen the "chop" block len of original file in byte
 * @return
 */
int getTotalBlockNum(int fileLenByte, int blockLen) {
    return fileLenByte / blockLen + (fileLenByte % blockLen != 0);
}

/**
 * Map character ascci to integer value from 0-97, since we only deal with tab, newline, and ascii 32-126,
 * there is no need to use a 128 len array.
 * @param c
 * @return
 */
int charIndexMap(char c) {
    if (c == 0b1001) { return 0; } // tab
    if (c == 0b1010) { return 1; } // \n
    if (c == 0b1101) { return 2; } // \r
    return c - 0b11101; // -29 so ascii 32 become 3 and so on...
}

/**
 * convert position index back to char (posi 0 - 97)
 * @param posi
 * @return
 */
char indexToCharMap(int posi) {
    if (posi == 0) { return '\t'; }
    if (posi == 1) { return '\n'; }
    if (posi == 2) { return '\r'; }
    return (char) (posi + 29);
}

/**
 * load a block of bwt from original file to buffer,
 * @param buffer
 * @param blockNum
 */
void loadBWTBlockToBuffer(char *buffer, std::ifstream &in, int blockNum) {
    readToBuffer(buffer, in, blockNum * BlockLengthInByte, BlockLengthInByte);
}

/**
 *
 * @param blockNum
 * @param buffer
 * @param in
 * @param indexTable
 */
void buildBlockIndex(const int blockNum, char *buffer, std::ifstream &in, int *indexTable) {
    //load count from last block
    if (blockNum > 0) {
        for (int i = 0; i < INDEX_ALPAHBET_SIZE; ++i) {
            indexTable[blockNum * INDEX_ALPAHBET_SIZE + i] = getIndexCharCountByInt(blockNum - 1, indexTable, i);
        }
    }

    loadBWTBlockToBuffer(buffer, in, blockNum);
    for (int i = 0; i < BlockLengthInByte; ++i) {
        if (buffer[i] == BUFFER_TERMIN) { break; }
        int position = charIndexMap(buffer[i]); // the position in index
        indexTable[blockNum * INDEX_ALPAHBET_SIZE + position] += 1;
    }

}

/**
 * Return the number of char counted on a block of index for a given ch
 * @param blockNum the ith block from 0
 * @param indexTable  the index table
 * @param ch the char looking for count
 * @return
 */
int getIndexCharCount(int blockNum, int *indexTable, char ch) {
    int position = charIndexMap(ch);
    return indexTable[blockNum * INDEX_ALPAHBET_SIZE + position];
}

int getIndexCharCountByInt(int blockNum, int *indexTable, int position) {
    return indexTable[blockNum * INDEX_ALPAHBET_SIZE + position];
}


/**
 *
 * @param lastBlockIndex
 * @param indexTable
 * @param Ctable
 */
void buildCtable(int lastBlockIndex, int *indexTable, int *Ctable) {
    int sum = 0;
    bool firstAppear = true;
    for (int i = 0; i < INDEX_ALPAHBET_SIZE; ++i) {
        int count = indexTable[lastBlockIndex * INDEX_ALPAHBET_SIZE + i];
        if (count != 0) {
            if (firstAppear) {
                Ctable[i] = 0;
                firstAppear = false;
            } else {
                Ctable[i] = sum;
            }
            sum += count;
        }
    }
}

/**
 * Look up the Ctable, and find the C for given character, returns -1
 * for non matching character
 * @param ch
 * @param Ctable
 * @return
 */
int getC(char ch, int *Ctable) {
    return Ctable[charIndexMap(ch)];
}

/**
 * look up the current char's next char's (the next existing char)'s C value, for the last char in C table,
 * it returns the length of bwt string of that block. if c does not exist, return -1
 * @param ch
 * @param Ctable
 * @return
 */
int nextC(char ch, int *Ctable, int lastBlockIndex, int *indexTable) {
    const int chPosition = charIndexMap(ch) + 1; // could be (0 - 98) // but 98 is out of bound
    // numMapToChar[chPosition - 1]
    if (chPosition == INDEX_ALPAHBET_SIZE) {
        // the last char in Alphabet table's next is being asked
        return Ctable[chPosition - 1] + getIndexCharCount(lastBlockIndex, indexTable, '~');
    }
    //getIndexCharCount(lastBlockIndex,indexTable,'~');
    //bool lastCharInC = false;
    for (int i = chPosition; i < INDEX_ALPAHBET_SIZE; ++i) {
        if (Ctable[i] != -1) {
            return Ctable[i];
        } else if (Ctable[i - 1] != -1) {
            // the last char in C table's next is being asked
            // nextC(lastChar) should be the C[lastChar] + number of lastChar in file
            return Ctable[i - 1] + getIndexCharCount(lastBlockIndex, indexTable, indexToCharMap(i - 1));
        } else {
            break;
        }
    }
    return -1;
}

/**
 * Get the block number and relative position in that block in bwt for a given absolute position in whole bwt
 * one assistant for occ function
 * @param position
 * @return
 */
std::tuple<int, int> getBlockAndRelativePosition(int position) {
    return std::make_tuple(position / BlockLengthInByte, position % BlockLengthInByte);
}

/**
 *
 * Linear scan the bwt block, find the occurance <= relative position
 * @param ch
 * @param relativePosi
 * @param buffer
 * @param in
 * @param blockNum
 * @return
 */
int occBlockLinear(char ch, int relativePosi, char *buffer, std::ifstream &in, int blockNum) {
    if (BIG_FILE) {
        loadBWTBlockToBuffer(buffer, in, blockNum);
    }
    int count = 0;
    for (int i = 0; i <= relativePosi; ++i) {
        if (buffer[i] == BUFFER_TERMIN) { break; }
        if (buffer[i] == ch) { ++count; }
    }
    return count;
}

/**
 * Return how many characters ch has occurred on block (indexBlockNum - 1), so occAll can combine the
 * result from here with the block linear scan
 * @param ch
 * @param indexBlockNum
 * @param indexTable
 * @return
 */
int occOnIndex(char ch, int indexBlockNum, int *indexTable) {
    if (indexBlockNum == 0) { return 0; }
    return getIndexCharCount(indexBlockNum - 1, indexTable, ch);
}

/**
 * The OCC function in backward search, return the number of occurance
 * @param ch
 * @param absPosi
 * @param buffer
 * @param in
 * @param indexTable
 * @return
 */
int occAll(char ch, int absPosi, char *buffer, std::ifstream &in, int *indexTable) {
    auto tup = getBlockAndRelativePosition(absPosi);
    int blockNum = std::get<0>(tup);
    int relativePos = std::get<1>(tup);
    if (BIG_FILE) {
        loadBWTBlockToBuffer(buffer, in, blockNum);
    }
    int occl = occBlockLinear(ch, relativePos, buffer, in, blockNum);
    int idxOcc = occOnIndex(ch, blockNum, indexTable);
    return occl + idxOcc;
}

/**
 * Do the backward search based on the material disccussed in the lecture. return the matching postion first and last
 * in tuple where tuple(0) is "first" and tuple(1) is "second". If no match, both of them will be -1.
 * @param term the search term
 * @param in original bwt file file stream
 * @param blockNum total number of block
 * @param indexTable the table storing index
 * @param Ctable the Ctable to compute C
 * @param buffer the buffer storing a block of character from bwt file
 * @return tuple(0) is "first" and tuple(1) is "second". negative value for no match.
 */
std::tuple<int, int>
backWardSearch(std::string term, std::ifstream &in, int totalblockNum, int *indexTable, int *Ctable, char *buffer) {
    if (term == "") {
        return std::make_tuple(-1, -1);
    }
    int posi = static_cast<int> (term.length() - 1);
    char c = term[posi];
    int firsPointer = getC(c, Ctable);
    int lastPointer = nextC(c, Ctable, totalblockNum - 1, indexTable) - 1;
    if (firsPointer < 0 || lastPointer < 0) { return std::make_tuple(-1, -1); }
    while (firsPointer <= lastPointer && posi >= 1) {
        c = term[posi - 1];
        int cCount = getC(c, Ctable);
        if (cCount < 0) { // char not exist in the whole bwt file
            return std::make_tuple(-1, -1);
        }
        firsPointer = cCount + occAll(c, firsPointer - 1, buffer, in, indexTable);
        lastPointer = cCount + occAll(c, lastPointer, buffer, in, indexTable) - 1;
        --posi;
    }

    if (lastPointer < firsPointer) {
        return std::make_tuple(-1, -1);
    }
    return std::make_tuple(firsPointer, lastPointer);
}

/**
 *
 * @param posi
 * @param in
 * @param Ctable
 * @param buffer
 * @param indexTable
 * @return a tuple containing: the decoded char for this position and the next char's position
 */
std::tuple<char, int> backwardOneChar(int posi, std::ifstream &in, int *Ctable, char *buffer, int *indexTable) {
    char ch = readCharInfile(in, posi);
    int nextCharPosi = getC(ch, Ctable) + occAll(ch, posi, buffer, in, indexTable) - 1;
    return std::make_tuple(ch, nextCharPosi);
}

/**
 * Do the backward decode after bwt backward search, returns empty string if the decode position is inside []
 * @param starPosi the position in bwt row where you what to start decode, should be the backward search returned value
 * @param in
 * @param Ctable
 * @param buffer
 * @param indexTable
 * @return
 */
std::string backwardDecode(int starPosi, std::ifstream &in, int *Ctable, char *buffer, int *indexTable) {
    std::string str = "";
    bool sawClosingBracket = false;
    while (true) {
        auto tup = backwardOneChar(starPosi, in, Ctable, buffer, indexTable);
        char ch = std::get<0>(tup);
        starPosi = std::get<1>(tup);
        if (ch == ']') {
            sawClosingBracket = true;
        }
        if (ch == '[') {
            if (sawClosingBracket) {
                return '[' + str;
            }
            // you are decoding id
            return "";
        }
        str = ch + str;
    }
    return str;
}

/**
 * Perform binary search on index table, trying to find the block contains the rank of that char in
 * (invertocc). Due to the index building scheme, this function returns the exact match of that rank (delta)
 * does not mean the block is correct because the correct block may happens before this block.
 * If the block rank does not match, then it is the correct one.
 * @param ch
 * @param rank
 * @param in
 * @param indexTable
 * @param totalNumBlock
 * @return tuple (block index, match number)
 */
int binarySearch(const char ch, int rank, int *indexTable, int totalNumBlock) {
    int low = 0;
    int high = totalNumBlock - 1;
    int countOfBlock = 0;
    while (low <= high) {
        int mid = low + (high - low) / 2;
        countOfBlock = getIndexCharCount(mid, indexTable, ch);
        if (rank < countOfBlock) {
            high = mid - 1;
        } else if (rank > countOfBlock) {
            low = mid + 1;
        } else {
            return mid;
        }
    }
    return low;
}

/**
 * The invertocc function used for forward decoding, returns the position of deltath a char in bwt original file
 * @param ch the char
 * @param delta the order of char. (ch=a delta=2 means looking for 2nd 'a' in bwt file)
 * @param in bwt file's file stream
 * @param buffer the buffer storing character's for a block
 * @param indexTable store index
 * @param totalNumBlock
 * @return
 */
int invertOcc(const char ch, int delta, std::ifstream &in, char *buffer, int *indexTable, int totalNumBlock) {
    int blockIndex = binarySearch(ch, delta, indexTable, totalNumBlock);
    int prevCount = 0;
    // look up before until find the first block containing delta characters
    for (; blockIndex >= 0; --blockIndex) {
        if (blockIndex == 0) {
            prevCount = 0;
            break;
        }
        prevCount = getIndexCharCount(blockIndex - 1, indexTable, ch);
        if (prevCount < delta) {
            break;
        }
    }
    // now get the real blockIndex
    // read that block
    loadBWTBlockToBuffer(buffer, in, blockIndex);
    int count = prevCount; // the before block's count must be added as a base
    int relativePosi = 0;
    for (; relativePosi <= BlockLengthInByte; ++relativePosi) {
        if (buffer[relativePosi] == BUFFER_TERMIN) {
            //should't be here !
            break;
        }
        if (buffer[relativePosi] == ch) {
            ++count;
            if (count == delta) {
                break;
            }
        }
    }

    return blockIndex * BlockLengthInByte + relativePosi;
}

/**
 * Get the char in the "First" column by a given absolute position in bwt file
 * @param absPosi
 * @param Ctable
 * @return
 */
char getF(int absPosi, int *Ctable) {
    char lastChar = '\0';
    char nextChar;
    for (int i = 0; i < INDEX_ALPAHBET_SIZE; ++i) {
        if (Ctable[i] == -1) { continue; }
        nextChar = indexToCharMap(i);
        if (Ctable[i] == absPosi) { // exact match
            return indexToCharMap(i);
        }
        if (Ctable[i] > absPosi) {
            return lastChar;
        }
        lastChar = nextChar;
    }
    return lastChar;
}

/**
 * forward decode by one character
 * @param posi
 * @param totalNumBlock
 * @param Ctable
 * @param buffer
 * @param in
 * @param indexTable
 * @param totalBlockNum
 * @return
 */
std::tuple<char, int>
forwardOneChar(int posi, int totalNumBlock, int *Ctable, char *buffer, std::ifstream &in, int *indexTable) {
    char ch = getF(posi, Ctable);
    int delta = posi - getC(ch, Ctable) + 1;
    int nextChPosi = invertOcc(ch, delta, in, buffer, indexTable, totalNumBlock);
    return std::make_tuple(ch, nextChPosi);
}

/**
 *
 * @param position start position
 * @param totalNumBlock
 * @param in
 * @param Ctable
 * @param buffer
 * @param indexTable
 * @return
 */
std::string
forwardDecode(int position, int totalNumBlock, std::ifstream &in, int *Ctable, char *buffer, int *indexTable) {
    std::string resultStr = "";
    while (true) {
        auto resTup = forwardOneChar(position, totalNumBlock, Ctable, buffer, in, indexTable);
        char ch = std::get<0>(resTup);
        position = std::get<1>(resTup);
        if (ch == ']') {
            return "";
        }
        if (ch == '[') {
            return resultStr;
        } else {
            resultStr += ch;
        }
    }
    // should be here
    return resultStr;
}

/**
 * real match (eliminate id in [])
 * @param fullText
 * @param substr
 * @return
 */
bool subStringMatch(const std::string &fullText, const std::string &substr) {
    auto rightBracketPosi = fullText.find("]");
    auto found = fullText.find(substr, rightBracketPosi + 1);
    if (found == std::string::npos) {
        return false;
    }
    return true;
}

/**
 * Get 3 bwt pointer pairs result, return the one that has smallest search range
 * @param termOneFirst
 * @param termOneLast
 * @param termTwoFirst
 * @param termTwoLast
 * @param termThreeFirst
 * @param termThreeLast
 * @return <theFirst,theLast,the term index for this minium area(-1 is no match at all)>
 */
std::tuple<int, int, int> getMinimumSearchArea(int termOneFirst, int termOneLast, int termTwoFirst,
                                               int termTwoLast, int termThreeFirst, int termThreeLast) {
    std::vector<std::tuple<int, int, int>> areas;
    if ((termOneFirst >= 0) && (termOneLast - termOneFirst >= 0)) {
        areas.push_back(std::make_tuple(termOneFirst, termOneLast, 0));
    }

    if ((termTwoFirst >= 0) && (termTwoLast - termTwoFirst >= 0)) {
        areas.push_back(std::make_tuple(termTwoFirst, termTwoLast, 1));
    }

    if ((termThreeFirst >= 0) && (termThreeLast - termThreeFirst >= 0)) {
        areas.push_back(std::make_tuple(termThreeFirst, termThreeLast, 2));
    }

    if (areas.size() == 0) { //all of them is given but non is searched
        return std::make_tuple(-1, -1, -1);
    }
    // sort by area length
    std::sort(std::begin(areas), std::end(areas),
              [](const std::tuple<int, int, int> &x, const std::tuple<int, int, int> &y) {
                  return (std::get<1>(x) - std::get<0>(x)) < (std::get<1>(y) - std::get<0>(y));
              });
    return areas[0];
}