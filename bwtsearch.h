#include <iostream>
#include <fstream>
int getFileLenByte(std::ifstream &in);

bool validateString(const std::string &s);

void loadIndex(std::string &indexFilePath, int blockCount, int *indexTable);

void saveIndexToFile(const std::string & indexFileName, int totalBNum, int *indexTable);

int getBlockLengthToDivide(const int fLen);

void readToBuffer(char *buffer, std::ifstream &in, int offset, int bufferSize);

char readCharInfile(std::ifstream &in, int absPosi);

int getTotalBlockNum(int fileLenByte, int blockLen);

int charIndexMap(char c);

char indexToCharMap(int posi);

void loadBWTBlockToBuffer(char *buffer, std::ifstream &in, int blockNum);

void buildBlockIndex(const int blockNum, char *buffer, std::ifstream &in, int *indexTable);

int getIndexCharCount(int, int *, char);

int getIndexCharCountByInt(int, int *, int);

//debug
void printCount(int blockIndex, int *indexTable);

void buildCtable(int lastBlockIndex, int *indexTable, int *Ctable);

void initIntArray(int *array, int defaultValue, int len);

void resetBuffer(char *buffer, int len);

//debug
void printCtable(int *Ctable);

int getC(char ch, int *Ctable);

int nextC(char ch, int *Ctable, int lastBlockIndex, int *indexTable);

std::tuple<int, int> getBlockAndRelativePosition(int position);

// occAll = occBlockLiner + occOnIndex
int occAll(char ch, int absPosi, char *buffer, std::ifstream &in, int *indexTable);

int occBlockLinear(char ch, int relativePosi, char *buffer, std::ifstream &in, int blockNum);

int occOnIndex(char ch, int indexBlockNum, int *indexTable);

std::tuple<int, int>
backWardSearch(std::string term, std::ifstream &in, int blockNum, int *indexTable, int *Ctable, char *buffer);

std::tuple<char, int> backwardOneChar(int posi, std::ifstream &in, int *Ctable, char *buffer, int *indexTable);

std::string backwardDecode(int posi, std::ifstream &in, int *Ctable, char *buffer, int *indexTable);

int invertOcc(const char ch, int delta, std::ifstream &in, char *buffer, int *indexTable, int totalNumBlock);

int binarySearch(const char ch, int rank, int *indexTable, int totalNumBlock);

std::tuple<char,int> forwardOneChar(int posi, int totalNumBlock, int *Ctable, char *buffer, std::ifstream &in, int *indexTable);

char getF(int absPosi, int * Ctable);

std::string
forwardDecode(int startPosi, int totalNumBlock, std::ifstream &in, int *Ctable, char *buffer, int *indexTable);

bool subStringMatch(const std::string &fullText, const std::string &substr);

std::tuple<int, int, int> getMinimumSearchArea(int termOneFirst, int termOneLast, int termTwoFirst,
                                         int termTwoLast, int termThreeFirst, int termThreeLast);



