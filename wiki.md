First, Get file length decide if it is small file or big file
###small file (<=2MB)
- if char count in file (length) <= 2MB, build index for every 512 chars, 
meaning the max index size is (2MB/512) * 128 * 4B = 2MB
- if the built index size larger than file, don't write 
to file, otherwise write.
- file with char len <=512 (REALLY REALLY small!) will have one 
index block that can help with finding C[i],
###big file
- file size (2MB,160MB]
- build a block of index for every 20480 characters
- write index to file after building

load index:
```c++
    std::string file = "./testfiles/test.idx";
    int * table = new int[2*INDEX_ALPAHBET_SIZE];
    loadIndex(file,2,table);
    outputTableRow(table,0);
    outputTableRow(table,1);
```
check c working
```bazaar
    //printCtable(Ctable);
/// /    std::cout<<"next[2] is: "<<nextC('2',Ctable)<<std::endl;
//    std::cout<<"next[[] is: "<<nextC('[',Ctable)<<std::endl;
//    std::cout<<"next[]] is: "<<nextC(']',Ctable)<<std::endl;
//    std::cout<<"next[a] is: "<<nextC('a',Ctable)<<std::endl;
//    std::cout<<"next[b] is: "<<nextC('b',Ctable)<<std::endl;
//    std::cout<<"next[n] is: "<<nextC('n',Ctable)<<std::endl;
    std::cout<<"C[nl] is: "<<getC('\n',Ctable)<<std::endl;
    std::cout<<"C[tab] is: "<<getC('\t',Ctable)<<std::endl;
    std::cout<<"C[cr] is: "<<getC('\r',Ctable)<<std::endl;
    std::cout<<"C[2] is: "<<getC('2',Ctable)<<std::endl;
    std::cout<<"C[[] is: "<<getC('[',Ctable)<<std::endl;
    std::cout<<"C[]] is: "<<getC(']',Ctable)<<std::endl;
    std::cout<<"C[a] is: "<<getC('a',Ctable)<<std::endl;
    std::cout<<"C[b] is: "<<getC('b',Ctable)<<std::endl;
    std::cout<<"C[n] is: "<<getC('n',Ctable)<<std::endl;
    std::cout<<"C[x] is: "<<getC('x',Ctable)<<std::endl;
    std::cout<<"C[q] is: "<<getC('x',Ctable)<<std::endl;
```

occ(char s,absPosition, blocksize) 
-> occBlockScan(s , blockRelativePosition, blockBuffer)


```bazaar
 for(int i=0; i < 9; ++i){
        int count = occAll('[',i, globalBuffer, is,indexTable);
        std::cout<<"occ([,"<<i<<") : "<<count<<std::endl;
    }

    for(int i=0; i < 9; ++i){
        int count = occAll(']',i, globalBuffer, is,indexTable);
        std::cout<<"occ(],"<<i<<") : "<<count<<std::endl;
    }

    for(int i=0; i < 9; ++i){
        int count = occAll('a',i, globalBuffer, is,indexTable);
        std::cout<<"occ(a,"<<i<<") : "<<count<<std::endl;
    }

    for(int i=0; i < 9; ++i){
        int count = occAll('b',i, globalBuffer, is,indexTable);
        std::cout<<"occ(b,"<<i<<") : "<<count<<std::endl;
    }

    for(int i=0; i < 9; ++i){
        int count = occAll('n',i, globalBuffer, is,indexTable);
        std::cout<<"occ(n,"<<i<<") : "<<count<<std::endl;
    }

    for(int i=0; i < 9; ++i){
        int count = occAll('2',i, globalBuffer, is,indexTable);
        std::cout<<"occ(2,"<<i<<") : "<<count<<std::endl;
    }

```
invert occ

```bazaar
    std::cout << "invert_occ(s,1): " << invertOcc('s', 1, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
    std::cout << "invert_occ(s,2): " << invertOcc('s', 2, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
    std::cout << "invert_occ(s,3): " << invertOcc('s', 3, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
    std::cout << "invert_occ(s,4): " << invertOcc('s', 4, is, globalBuffer, indexTable, totalBlockNum) << std::endl;

    std::cout << "invert_occ(i,1): " << invertOcc('i', 1, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
    std::cout << "invert_occ(i,2): " << invertOcc('i', 2, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
    std::cout << "invert_occ(i,3): " << invertOcc('i', 3, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
    std::cout << "invert_occ(i,4): " << invertOcc('i', 4, is, globalBuffer, indexTable, totalBlockNum) << std::endl;

    std::cout << "invert_occ(p,1): " << invertOcc('p', 1, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
    std::cout << "invert_occ(p,2): " << invertOcc('p', 2, is, globalBuffer, indexTable, totalBlockNum) << std::endl;

    std::cout << "invert_occ(m,1): " << invertOcc('m', 1, is, globalBuffer, indexTable, totalBlockNum) << std::endl;
```